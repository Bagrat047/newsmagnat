<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('id','DESC')->paginate(5);
        return view('admin.category-list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name'=>'required|min:3|max:255',
        ]);
        if($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $inputs = $request->except('_token');

        $new_category = new Category();
        $new_category->fill($inputs);
        if($new_category->save()){
            return redirect('/admin/category')->with('status', 'Category successfully added!!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_category = Category::find($id);
        return view('admin.category-edit',compact('edit_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name'=>'required|min:3|max:255',
        ]);
        if($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $inputs = $request->except('_token', '_method');

        $edit_category = Category::find($id);
        $edit_category->fill($inputs);
        if($edit_category->update()){
            return redirect('/admin/category')->with('status', 'Service successfully added!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Category::find($id);
        $destroy->delete();
        return redirect('/admin/category')->with('status', 'Category successfully delete!!!');
    }
}

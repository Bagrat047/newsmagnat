<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\NewsComment;
use App\NewsLike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('id', "DESC")->paginate(6);
        $categories = Category::orderBy('id', "DESC")->paginate(10);

        $userLikedNewsArray = NewsLike::select('news_id')->where('user_id', Auth::id())->get()->toArray();
        $userLikedNews = [];
        foreach ($userLikedNewsArray as $item) {
            $userLikedNews[] = $item['news_id'];
        }

        $userDislikedNewsArray = NewsLike::select('news_id')->where('user_id',Auth::id())->get()->toArray();
        $userDislikedNews = [];
        foreach ($userDislikedNewsArray as $item){
            $userDislikedNews[] = $item['news_id'];
        }

       /* $userCommentedNewsArray = NewsComment::select('news_id')->where('user_id',Auth::id())->get()->toArray();
        $userCommentedNews = [];
        foreach ($userCommentedNewsArray as $item){
            $userCommentedNews = $item['news_id'];
        }*/

        return view('admin.news-list', compact('news','categories','userLikedNews','userDislikedNews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.news-add' , compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $valid = Validator::make($request->all(), [

            'image' => 'image|mimes:jpeg,jpg,png|max:2048',
            'title' => 'required|max:50'
        ]);
        if ($valid->fails()){
            return redirect()->back()->withErrors($valid);
        }



        $inputs = $request->except('_token' , 'categories');
        $news = new News();
        if($request->hasfile('image')) {

            $file = $request->file('image');
            //$name =  $file->getClientOriginalName();
            $name = time().'.'.$file->extension();


            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(300, ' ', function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('/images/' .$name));



            $file->move(public_path() . '/images/' , 'original' . $name);
            $inputs['image'] = $name;

        }
        $categories = $request->category_id;
        $news->fill($inputs);
        if($news->save()){
            $news->category()->attach($categories);

            return redirect('/admin/news')->with('status', 'News successfully added!!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $destroy = News::find($id);
        $destroy->delete();
        return redirect('/admin/news')->with('status', 'News successfully delete!!!');
    }

    /*
<<<<<<< HEAD
    public function likeNews(Request $request) {
        $bool = false;
        $news_id = $request->news_id ;
        if($news_id) {
=======
    */
    public function likeNews(Request $request, $id) {
        $bool = false;
        $news_id = $id;
        if ($news_id){
//>>>>>>> origin/new_branch
            $like = new NewsLike();
            $like->news_id = $news_id;
            $like->user_id = Auth::id();
            if($like->save()) {
                $bool = true;
            }
        }
        return response()->json([
            'success' => $bool, 'id' => $id
        ]);
    }
    /*
<<<<<<< HEAD

    public function dislikeNews(Request $request){
        $bool = false;
        $news_id = $request->news_id ;
        if($news_id) {
            $dislike =NewsLike::where('news_id' , $news_id)->where('user_id', Auth::id());
=======
    */
    public function dislikeNews(Request $request, $id){
        $bool = false;
        $news_id = $id;
        if ($news_id){
            $dislike =NewsLike::where('news_id',$news_id)->where('user_id',Auth::id());
            /*$dislike->news_id = $request;
            $dislike->user_id = Auth::id();*/
//>>>>>>> origin/new_branch
            if ($dislike->delete()){
                $bool = true;
            }
        }

        return response()->json([
            'success' => $bool , 'id' => $id
        ]);
    }
    public function commentNews($id){
        $comment_news = News::find($id);

        return view('admin.news-comment',compact('comment_news'));
    }
}

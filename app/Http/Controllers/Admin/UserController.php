<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\News;
use App\User;
use Validator;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use mysql_xdevapi\Table;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;



class UserController extends Controller
{
    public function registered()
    {
        $users = User::paginate(10);

        return view('admin.user-list')->with('users', $users);

    }

    public function registeredit(Request $request, $id)
    {
        $users = User::findOrFail($id);
        return view('admin.user-edit')->with('users', $users);
    }

    public function registerupdate(Request $request, $id)
    {
       // dd('m');
        $users = User::find($id);
        $users->name = $request->input('name');
        $users->role = $request->input('role');
        $users->update();

        return redirect('/admin/user');

    }

    public function search(Request $request)
    {
        $categories = Category::orderBy('id', 'DESC');
        // $search = $request->get('search');
        $q = Input::get('q');
        if ($q != '') {
            $user = News::where('title', 'LIKE', '%' . $q . '%')->orWhere('description', 'LIKE', '%' . $q . '%')->get();
            if (count($user) > 0) {
                return view('admin.news-list', compact('categories'))->withDetails($user)->withQuery($q);
            }
            return view('admin.news-list', compact('categories'))->withMessage("No data");

        }


    }

    public function findCategory(Request $request)
    {
       /* if ($_GET['from'] < $_GET['to']){
            dd('m');
        }*/
        //dd($_GET['from']);
        //dd($request->all());
        //dd('m');

        $valid =Validator::make($request->all(), [
            'from' => 'date',
            'to' => 'date|after_or_equal:from',
        ]);
        if ($valid->fails()){
            return redirect()->back()->withErrors($valid);
        }

        $categories =Category::all();
        $role = $request->input('role');

          $news = News::whereHas('category', function ($query) use ($role){
              $query->where('category_id', $role);
          })->where('created_at', '>=', $_GET['from'])->where('created_at','<=', $_GET['to'])
              ->paginate(6);
        //$news = $categories ->news;
//        dd($news);

          return view('admin.news-list', compact('news','categories'));

        //return view('admin.news-list', compact('categories', 'news'));
       // dd($categories);





       /* $q = Input::get('q');
        if ($q != '') {
            $news = News::where('categories', 'LIKE', '%' . $q . '%')->get();
            return view('admin.news-list')->withDetails($news)->withQuery($q);
        }*/

    }

    public function index()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $arr_ip = geoip()->getLocation($ip);
        dd($arr_ip);

    }

    public function ffmpeg($id)
    {
        /* $name = time();
            FFMpeg::fromDisk('local')
                 ->open('Zlatan.mp4')

                    ->addFilter(function ($filters) {
                        $filters->resize(new \FFMpeg\Coordinate\Dimension(1380, 820));
                    })
                ->getFrameFromSeconds(54.1)
                 ->export()
                 ->toDisk('public')
                ->inFormat(new \FFMpeg\Format\Video\X264)
                 ->save("new$name.png");*/


        /* $name = time();
         FFMpeg::fromDisk('local')
             ->open('FrameAt10sec.png')
 //                ->addFilter('-r', 60)
                 ->addFilter(['-f', 'image2'])
                 ->getFrameFromSeconds(54.1)
             //->addFilter('-r', 60)
             //->addFilter(['-f', 'image2'])
             ->export()
             ->toDisk('public')
             ->save("$name.png");*/


        /* ->export()
         ->toDisk('public')
         ->save("new$name.png");*/


        // $img = \Intervention\Image\Facades\Image::make(public_path() . '/new1574168086.png')->blur(10)->resize(600,600);

        // return $img->response('jpg');
        // dd('m');
        $news = News::find($id);
        $image = public_path('/images/' . $news->image);
        $blur = \Intervention\Image\Facades\Image::make($image)->blur(50)->resize(600, 600)
            ->save(public_path('/images/' . $news->image));
//        return $blur->response('jpg');
        return redirect()->back();

        //$img->delete();
        //return redirect('/admin/news');


    }



    public function feedbackindex(){
        return view('admin.feedback-list');
    }
    function send(Request $request)
    {

        $this->validate($request,[
            'title' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        $data = array(
            'title' => $request->title,
            'message' => $request->message
        );
        //dd($data);
        $email = $request->email;
        //dd($email);
        Mail::send(['text'=>'email_template', 'data' => $data], $data, function($message) {
            //vd
            $message->to('bagratghonyan047@gmail.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
            $message->from('bagratghonyan047@gmail.com','Virat Gandhi');
        });
//        Mail::to('admin2@gmail.com')->send(new SendMail($data));
       // dd($email);

        return back()->with('success', 'Thanks');
        /*   MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null*/


    }

}

<?php

namespace App\Http\Controllers;

use App\NewsComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request,$news){

        $comment = new NewsComment();
        $comment->news_id = $news;

        $comment->user_id = Auth::id();
        $comment->comment = $request->comment;
       // dd($comment->comment,$comment->user_id);

        $comment->save();
        return redirect()->back();

    }
    public function delete($id){
        dd('test');
        $destroy = NewsComment::find($id);
        $destroy->delete();
        return redirect()->back();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class News extends Model
{

    use SoftDeletes;


    protected $fillable = [
        'image', 'title','description'
    ];

    public function category(){
        return $this->belongsToMany('App\Category', 'news_categories');
    }

    protected $dates = ['deleted_at'];
///////
    public function comments(){
        return $this->hasMany('App\NewsComment');
    }
///////



}


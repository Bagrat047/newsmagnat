<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsLike extends Model
{
    protected $fillable = [
        'news_id', 'user_id',
    ];
    protected $table = 'news_like';
}

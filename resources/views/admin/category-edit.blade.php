@extends('layouts.app')

@section('title', 'Category add')

@section('page-name', 'Category add page')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <form action="{{ route('category.update', $edit_category->id) }}" method="post" role="form">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{ $edit_category->name}}" id="name" class="form-control" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>

@endsection
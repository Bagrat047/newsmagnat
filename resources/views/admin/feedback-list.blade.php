@extends('layouts.app')

@section('title', 'Feedback')

@section('page-name', 'Feedback page')

@section('content')
<div class="row">
    <div class="col-lg-12">
        {{--@if(count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss></button>
            </div>

        @endif--}}


        @if($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
            </div>
        @endif



            <form action="{{url('/admin/feedback/send')}}" method="post">
                @csrf
                <div class="form-group">
                    <label>Enter Title</label>
                    <input type="text" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label>Enter Email</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Enter Message</label>
                    <textarea name="message" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <input type="submit" name="send" value="Send" class="btn btn-info">
                </div>

            </form>


    </div>

</div>

@endsection

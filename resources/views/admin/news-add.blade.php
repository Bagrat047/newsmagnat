@extends('layouts.app')

@section('title', 'News')

@section('page-name', 'News add page')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <form action="{{ route('news.store') }}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="image">New image</label>
                    <input type="file" name="image" id="image"  class="filestyle" data-buttonBefore="true" data-buttonName="btn-primary">
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" value="{{ old('title') }}" id="title" class="form-control" placeholder="Enter Name">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" id="description">{{ old('description') }}</textarea>
                </div>
                @if(isset($categories) && is_object($categories))
                    <div class="form-group">
                        <label for="category_id">Enter Category</label>
                        <select name="category_id[]" multiple id="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Add News</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('text');
    </script>
@endsection
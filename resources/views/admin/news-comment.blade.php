@extends('layouts.app')

@section('title', 'News')

@section('page-name', 'News comment page')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <form action="{{ route('comment.store', $comment_news->id)}}" method="post">
                @csrf
                <input type="hidden" name="news_id" value="{{ $comment_news->id }}">

                <div class="row">
                    <div class="col-sm-12">
                        <label for="id">Id:</label>
                        {{$comment_news->id}}
                        <br>
                        <label for="title">Title:</label>
                        {{$comment_news->title}}
                        <textarea name="comment" rows="2" class="text-area-messge form-control" placeholder="Enter your comment"></textarea>
                    </div>
                    <div class="col-sm-12">
                        <button class="btn-info" type="submit"><b>POST COMMENT</b></button>
                    </div>
                </div>

            </form>
            <h4><b>Comments({{ $comment_news->comments()->count() }})</b></h4>
            @if($comment_news->comments->count() > 0)
                @foreach($comment_news->comments as $comment)
                    <div class="midtext">
                        <b>{{ $comment->user->name }}</b>
                        <h6>{{ $comment->created_at->diffForHumans() }}</h6>
                    </div>
                    <div class="comment">
                        <p>{{ $comment->comment }}</p>
                        <form action="{{ url('/news/deleteComment',  ['id' => $comment->id]) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn-danger">Delete</button>
                        </form>

                    </div>
                    @endforeach
                @else
                <p>No comment yet.</p>
                @endif
        </div>
    </div>





@endsection

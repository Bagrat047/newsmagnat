@extends('layouts.app')

@section('title', 'News')

@section('page-name', 'News add page')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <form action="{{ route('news.update', $member_edit->id) }}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <p>Old Image :</p>
                @foreach($member_edit->path as $path)
                    <img src="{{ asset('images') . '/' . $path}}" alt="{{ $path}} " style="width: 100px;height: 100px;">
                    <input type="hidden" name="path" value="{{  $path }}">

                @endforeach
                <div class="form-group">
                    <label for="path">New image</label>
                    <input type="file" name="new_path" id="path"   class="filestyle" data-buttonBefore="true" data-buttonName="btn-primary">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{ $member_edit->name }}" id="name" class="form-control" placeholder="Enter Name">
                </div>
                <div class="form-group">
                    <label for="facebook_link">Facebook link</label>
                    <input type="url" name="facebook_link" value="{{ $member_edit->facebook_link }}" id="facebook_link" class="form-control" placeholder="Enter link">
                </div>
                <div class="form-group">
                    <label for="twitter_link">Twitter link</label>
                    <input type="url" name="twitter_link" value="{{ $member_edit->twitter_link }}" id="twitter_link" class="form-control" placeholder="Enter link">
                </div>
                <div class="form-group">
                    <label for="gplus_link">Google Plus link</label>
                    <input type="url" name="gplus_link" value="{{ $member_edit->gplus_link }}" id="gplus_link" class="form-control" placeholder="Enter link">
                </div>
                <div class="form-group">
                    <label for="ld_link">Linkedin link</label>
                    <input type="url" name="linkedin_link" value="{{ $member_edit->linkedin_link }}" id="linkedin_link" class="form-control" placeholder="Enter link">
                </div>
                @if(isset($professions) && is_object($professions))
                    <div class="form-group">
                        <label for="profession_id">Enter Category</label>
                        <select name="profession_id" id="profession_id" class="form-control">
                            <option value="">Choose Profession</option>
                            @foreach($professions as $profession)
                                @if( $member_edit->profession_id == $profession->id)
                                    <option value="{{$profession->id}}" selected>{{$profession->name}}</option>

                                @else
                                    <option value="{{$profession->id}}">{{$profession->name}}</option>

                                @endif

                            @endforeach
                        </select>
                    </div>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Edit Member</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('text');
    </script>
@endsection
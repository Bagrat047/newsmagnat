@extends('layouts.app')

@section('title', 'News')

@section('page-name', 'News list')
<!--
<<<<<<< HEAD

=======
-->
<!--
>>>>>>> origin/new_branch
-->
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All News list
                    <a href="{{ route('news.create') }}" class="btn btn-primary pull-right btn-sm" role="button"
                       style="margin: -5px;">Add new News</a>

                    <br><br>


@if(isset($categories))
                            <form action="{{url('/admin/news/findCategory/')}}" method="get">
                                <div class="form-group">
                                    <label>Find category</label>

                                    <div class="form-group">
                                        <label>FROM</label>
                                        <input type="date" name="from" class="form-control">

                                        <label>TO</label>
                                        <input type="date" name="to" class="form-control">

                                    </div>


                                        <select class="form-control" name="role">
                                            @foreach($categories as $category)
                                                @if($category->id == app("request")->input('role'))

                                                    <option selected="selected" value="{{$category->id}}">{{$category->name}}</option>
                                                    @else
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endif



                                            @endforeach
                                        </select>

                                </div>
                                <input type="submit" class="btn btn-primary" value="Find" >
                            </form>

                    @endif


                    <br><br>
                   {{-- @if(isset($details))
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($details as $news)
                                <tr>
                                    <td>{{ $news->title }}</td>
                                    <td>{{ $news->description }}</td>
                                    <td>
                                        <img src="{{asset("images/$news->image")}}" style="width: 100px; height: 100px;">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    @endif--}}









                    <form action=" {{url('/admin/news/search')}}" method="get" role="search">
                        <div class="input-group">

                            <input type="text" class="form-control" name="q" placeholder="Search users">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                        </div>
                    </form>
                    @if(isset($details))
                        <br>
                        Search result:
                    <br><br>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($details as $user)
                                    <tr>
                                        <td>{{ $user->title }}</td>
                                        <td>{{ $user->description }}</td>
                                        <td>
                                            <img src="{{asset("images/$user->image")}}" style="width: 100px; height: 100px;">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                        @elseif(isset($message))
                        <br>
                        {{ $message }}


                    @endif



                </div>
                <div class="panel-body" id="sortable">
                    @if(isset($news) && is_object($news))
                        <table width="100%" class="table table-striped table-bordered table-hover"
                               id="dataTables-example">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Categories</th>
                                <th>Delete</th>
                                <th>Ffmpeg</th>
                                <th>Like or Dislike</th>
                                <th>Comment</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($news as $item)
                                <tr class="odd gradeX">
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <img src="{{ asset('images') . '/' . $item->image}}" alt="{{ $item->image}} " style="width: 100px;height: 100px;">
                                    </td>
                                    <td>{{ $item->title }}</td>

                                    <td>

                                {{-- {{str_limit($item->description,10,'') }}
                                        @if(strlen($item->description)>10)
                                            <span id="dots">...</span>
                                            <span id="more">{{ substr($item->description,10) }}</span>
                                        @endif

                                            <button onclick="myFunction()" data-option="{{$item->id}}"  id="myBtn">Read more</button>
--}}
                                        @if(strlen($item->description) > 10)
                                            {{substr($item->description,0,10)}}
                                            <span class="read-more-show">...More</span>
                                            <span class="read-more-content hide_content"> {{substr($item->description,10,strlen($item->description))}}
                                                 <span class="read-more-hide ">Less</span>
                                            </span>
                                        @else
                                            {{$item->description}}
                                        @endif
                                        <style>
                                            .read-more-show{
                                                cursor:pointer;
                                                color: #2e6da4;
                                            }
                                            .read-more-hide{
                                                cursor:pointer;
                                                color: #ed8323;
                                            }

                                            .hide_content{
                                                display: none;
                                            }
                                        </style>


                                      {{--  <style>
                                            #more{
                                                display: none;
                                            }
                                        </style>--}}
                                    </td>

                                    @if(isset( $item->category ))
                                        <td>
                                        @foreach($item->category as $category)
                                            {{$category->name}} <br>
                                        @endforeach
                                        </td>

                                    @else
                                        <td> Null</td>
                                    @endif
                                    <td>
                                        <form action="{{ route('news.destroy', $item->id) }}" method="post" onsubmit="return confirm('Are you sure you want to send the service to the trash?')">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa-fw"></i></button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{ url('/admin/news/ffmpeg', $item->id) }}" method="post">
                                            @csrf

                                            <button type="submit" class="btn btn-primary btn-sm">Blur</button>
                                        </form>
                                    </td>

                                    <td>
                                        @if(in_array($item->id , $userLikedNews))
<!-- <<<<<<< HEAD

======= -->
                                            <button data-url="{{ route('disLikeNews', ['id' => $item->id]) }}" data-option="dislike" class="like btn btn-danger">Dislike</button>
                                        @else
                                            <button data-url="{{ route('likeNews', ['id' => $item->id]) }}" data-option="like" class="like btn btn-info">Like</button>
                                        @endif

                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/news/commentNews', $item->id) }}"><button type="submit" class="btn-info">Comment</button></a>
<!-- >>>>>>> origin/new_branch -->
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $news->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div id="load" data-sotr-order-image>
        <i class="fa fa-spinner fa-spin"></i>
    </div>
@endsection

@extends('layouts.app')

@section('title', 'User edit')

@section('page-name', 'User edit page')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">



            <form action="/admin/user-update/{{$users->id}}" method="post" role="form">

                @csrf
                <input type="hidden" name="_method" value="PUT">


                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{$users->name}}" id="name" class="form-control" placeholder="Enter name">
                </div>

                <div class="form-group">
                    <label>Give Role</label>
                    <select name="role" class="form-control">
                        <option value="1">Admin</option>
                        <option value="">None</option>
                    </select>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>




        </div>
    </div>

@endsection
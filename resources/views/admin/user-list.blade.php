@extends('layouts.app')

@section('title', 'User list')

@section('page-name', 'User list')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">



                <div class="panel-body" id="sortable">
                    @if(isset($users) && is_object($users))
                        <table width="100%" class="table table-striped table-bordered table-hover"
                               id="dataTables-example">

                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>role</th>
                                <th>Edit</th>

                            </tr>
                            </thead>



                            <tbody>
                            @foreach($users as $row)
                                <tr class="odd gradeX">
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->role }}</td>
                                    <td>
                                        <a href="/admin/user-edit/{{$row->id}}" class="btn btn-info btn-sm"><i class="fa fa-edit fa-fw"></i></a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>



                        </table>
                        {{ $users->links() }}
                    @endif
                </div>





            </div>
        </div>
    </div>

    <div id="load" data-sotr-order-image>
        <i class="fa fa-spinner fa-spin"></i>
    </div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role']], function () {

    Route::get('/', 'HomeController@admin');

    Route::get('/news/search', 'Admin\UserController@search');

    Route::get('/news/findCategory', 'Admin\UserController@findCategory');

/*<<<<<<< HEAD
    Route::get('/news/like', 'Admin\NewsController@likeNews')->name('likeNews');
    Route::get('/news/dislike', 'Admin\NewsController@disLikeNews')->name('disLikeNews');

=======
*/
    Route::get('/news/like/{id}', 'Admin\NewsController@likeNews')->name('likeNews');
    Route::get('/news/dislike/{id}', 'Admin\NewsController@disLikeNews')->name('disLikeNews');
    Route::get('/news/commentNews/{id}', 'Admin\NewsController@commentNews');
    //Route::post('comment/{news}','CommentController@store')->name('comment.store');
//>>>>>>> origin/new_branch

    Route::post('/news/comment/{news}','CommentController@store')->name('comment.store');
    Route::delete('/news/deleteComment/{id}','CommentController@delete')->name('comment.delete');

//  News
    Route::resource('/news', 'Admin\NewsController');

//    Category
    Route::resource('/category', 'Admin\CategoryController');

    Route::get('/user', 'Admin\UserController@registered');

    Route::get('/user-edit/{id}', 'Admin\UserController@registeredit');

    Route::put('/user-update/{id}', 'Admin\UserController@registerupdate');


    Route::get('/ip', 'Admin\UserController@index');


    Route::post('/news/ffmpeg/{id}', 'Admin\UserController@ffmpeg');
    Route::delete('/news/ffmpeg/{id}','Admin\UserController@ffmpeg');

    Route::get('/feedback', 'Admin\UserController@feedbackindex');
    Route::post('/feedback/send', 'Admin\UserController@send');



});
/*Route::get('/test', function () {
    $img = \Intervention\Image\Facades\Image::make(public_path() . '/new1574168086.png')->blur(80);

    return $img->response('jpg');
});*/
/*Route::group(['prefix'=>'admin','middleware'=>'auth'], function (){
    Route::get('/', function (){
        return "i am in user panel";
    });
});       */
